package europetweetsanalysis.utility.java;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ConverterUtils.DataSink;

public class FileManager {

	/**
	 * Read a file and return an arraylist containing all the file lines
	*/
	public static ArrayList<String> readStringsListFile(String filepath) {
		System.out.println("\n---------- Reading file: " + filepath + " -----------");
		ArrayList<String> stringsList = new ArrayList<String>();
		try (BufferedReader br = Files.newBufferedReader(Paths.get(filepath))) {
			String string;
			while ((string = br.readLine()) != null) {
				stringsList.add(string);
			}
		} catch (IOException e) {
			System.err.format("\nIOException: %s%n", e);
		}
		return stringsList;
	}

	/**
	 * Print a string in a file with or without doing append. 
	 * If the directory doesn't exist it is created. Same thing for the file
	*/
	public static void printStringToFile(String str, String outputFilepath, boolean append) {
		File folderPath = new File(outputFilepath.substring(0, outputFilepath.lastIndexOf("/") + 1));
		if (!folderPath.exists()) {
            folderPath.mkdirs();
            System.out.println("*******Directory created********");
        } 
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilepath, append));) {
			writer.write(str);
			System.out.println("\n----------File created: " + outputFilepath + " -----------");
		} catch (IOException e) {
			System.err.println("Failed to save data to: " + outputFilepath);
			e.printStackTrace();
		}
	}

	/**
	 * Given a file name, return an other file name concatenating the previous file
	 * name with the number of file in the folder. It is used during the writing in
	 * a folder, to not overwrite a file
	*/
	public static String getNewFilePath(String filePath) {
		String folderPath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
		String fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
		int fileCount = getNumberOfFile(folderPath);
		filePath = folderPath + String.valueOf(fileCount) + "_" + fileName;
		return filePath;
	}

	/** 
	 * Read an arff file and return a Instances object
	*/
	public static Instances readInstancesFromArff(String filepath) {
		Instances dataset = null;
		try (InputStreamReader ir = new InputStreamReader(new FileInputStream(filepath), "UTF-8");
				BufferedReader bufferedReader = new BufferedReader(ir);) {
			ArffReader arffReader = new ArffReader(bufferedReader);
			dataset = arffReader.getData();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.err.println("Failed to read data from: " + filepath);
			e.printStackTrace();
		}
		return dataset;
	}

	/** 
	 * Add file count in the file name to avoid overwriting of existing arff file. 
	 * If the file doesn't exist it is created
	*/
	public static void printInstancesToArff(Instances dataset, String outputFilepath) {
		try {
			outputFilepath = getNewFilePath(outputFilepath);
			DataSink.write(outputFilepath, dataset);
			System.out.println("\n----------File created: " + outputFilepath + " -----------");
		} catch (Exception e) {
			System.err.println("Failed to save data to: " + outputFilepath);
			e.printStackTrace();
		}
	}

	/*
	 * Read all file names in a folder that respect an input pattern, for example: ".*\\.arff". If the
	 * pattern is "*" return all files names
	 */
	public static String[] readAllFileName(String folderPath, String pattern) {
		File folder = new File(folderPath);
		System.out.println(folderPath);
		List<String> filesNames = new ArrayList<>();
		for (File file : folder.listFiles()) {
			if (file.isFile()) {
				if ((pattern.compareTo("*") == 0) || (file.getName().matches(pattern))) {
					filesNames.add(file.getName());
				}
			}
		}
		return filesNames.toArray(new String[filesNames.size()]);
	}

	public static String[] readAllFileName(String folderPath) {
		return readAllFileName(folderPath, "*");
	}

	/**
	 * Get the number of file contained in a folder
	*/
	private static int getNumberOfFile(String directoryPath) {
		File folderPath = new File(directoryPath);
		if (!folderPath.exists()) {
			return 0;
		}
		File directory = new File(directoryPath);
		int fileCount = directory.list().length;
		return fileCount;
	}

}