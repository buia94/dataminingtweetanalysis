package europetweetsanalysis.utility.java;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BashExecutor {

    /**
     * Execute bash command and return the command output stream. The command error
     * stream is printed on the terminal if requested
     */
    public static final String executeScript(String command, boolean printErrorStream) {
        Process process;
        StringBuilder commandOutputStream = new StringBuilder("");
        StringBuilder commandErrorStream = new StringBuilder("");
        try {
            process = Runtime.getRuntime().exec(command);
            process.waitFor();
            BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line = "";
            while ((line = outputReader.readLine()) != null) {
                commandOutputStream.append(line + "\n");
            }

            if (printErrorStream) {
                BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                line = "";
                while ((line = errorReader.readLine()) != null) {
                    commandErrorStream.append(line + "\n");
                }
                System.out.println(commandErrorStream.toString());
                errorReader.close();
            }

            outputReader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return commandOutputStream.toString();
    }
}