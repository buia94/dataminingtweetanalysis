package europetweetsanalysis.collectionoftweets.java;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TweetQueryManager {

	final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Return the list of tweets containing the input keywords, bounded between the
	 * input dates and that respect input language.
	 * The search is repeated for sub-ranges of dates incrementally. 
	 * Because the advanced twitter search limits the number of tweets returned daily for too large intervals.
	 */
	public static List<Tweet> getTweetsByKeywords(ArrayList<String> keywordsList, String initialDate, String finalDate, String language, int maxTweets) {
		System.out.println("\n######## Get tweets by keywords list ########");
		
		int totalTweets = 0;
		int numTweetsForSingleKeyword = 0;
		List<Tweet> allTweetsList = new ArrayList<Tweet>();

		//To keep all tweets of the final date (because tweets with that final date have a different time zone)
		String incrementedFinalDate = getIncrementedDate(finalDate, 2);	

		String subIntervalInitialDate = initialDate;

		while(subIntervalInitialDate.compareTo(incrementedFinalDate)!=0){
			String subIntervalFinalDate = getIncrementedDate(subIntervalInitialDate, Parameters.NUMBER_OF_DAYS);
			if(subIntervalFinalDate.compareTo(incrementedFinalDate)>0){
				subIntervalFinalDate = incrementedFinalDate;
			}

			System.out.println("\n+++++++ Date interval: " + subIntervalInitialDate + "  " + subIntervalFinalDate + " +++++++\n");
			
			for (int i = 0; i < keywordsList.size(); i++) {
				List<Tweet> tweetListForSingleKeyword = getTweetsByQuerySearch(keywordsList.get(i), subIntervalInitialDate, subIntervalFinalDate, language, maxTweets);
				allTweetsList.addAll(tweetListForSingleKeyword);
				numTweetsForSingleKeyword = tweetListForSingleKeyword.size();
				totalTweets += numTweetsForSingleKeyword;
				System.out.println("\nFound : " + numTweetsForSingleKeyword + " tweets\n");
			}

			subIntervalInitialDate = subIntervalFinalDate;
			System.out.println("+++++++ End date interval +++++++\n");
		}
		
		System.out.println("\n########## Found globally: " + totalTweets + " tweets #########\n");

		String[] datesToRemove = {getIncrementedDate(initialDate, -1), incrementedFinalDate, getIncrementedDate(finalDate, 1)};
		List<Tweet> allTweetsAfterDatesRemoval = removeDatesFromTweets(allTweetsList, datesToRemove);

		return allTweetsAfterDatesRemoval;
	}

	/**
	 * Return the list of tweets containing the query seacrh, bounded between the
	 * input dates and that respect input language
	 */
	public static List<Tweet> getTweetsByQuerySearch(String querySearch, String initialDate, String finalDate, String language, int maxTweets) {
		List<Tweet> tweetList;

		TwitterCriteria criteria = TwitterCriteria.create().setQuerySearch(querySearch).setSince(initialDate).setUntil(finalDate);
		
		if (maxTweets > 0) {
			criteria.setMaxTweets(maxTweets);
		}
		if (language != "all") {
			criteria.setLanguage(language);
		}

		System.out.println("\n### Get tweets by query search [" + querySearch + "]");
		System.out.println("Searching... \n");
		tweetList = TweetDownloader.getTweets(criteria);
		return tweetList;
	}

	/**
	 * Return the list of tweets containing an input username, limited between input
	 * dates and limiting the number of tweets extracted
	 */
	public static List<Tweet> getTweetsByUsernameAndBoundDates(String username, String initialDate, String finalDate, int maxTweets) {
		List<Tweet> tweetList;
		TwitterCriteria criteria = TwitterCriteria.create().setUsername(username).setSince(initialDate).setUntil(finalDate);
		if (maxTweets > 0) {
			criteria.setMaxTweets(maxTweets);
		}
		System.out.println("### Get tweets by username and bound dates [" + username + ", " + initialDate + ", "
				+ finalDate + "]");
		System.out.println("Searching... \n");
		tweetList = TweetDownloader.getTweets(criteria);
		return tweetList;
	}

	/**
	 * Return the list of tweets containing an input username, limiting the number
	 * of tweets extracted
	 */
	public static List<Tweet> getTweetsByUsername(String username, int maxTweets) {
		List<Tweet> tweetList;
		TwitterCriteria criteria = TwitterCriteria.create().setUsername(username);
		if (maxTweets > 0) {
			criteria.setMaxTweets(maxTweets);
		}
		System.out.println("### Get tweets by username " + username);
		System.out.println("Searching... \n");
		tweetList = TweetDownloader.getTweets(criteria);
		return tweetList;
	}

	private static String getIncrementedDate(String dateToIncrement, int numberOfDays){
		 Date dateToIncrementObject = getDateObject(dateToIncrement);
		 Calendar c = Calendar.getInstance();

		 c.setTime(dateToIncrementObject);
 
		 c.add(Calendar.DATE, numberOfDays); 
 
		 Date incrementedDate = c.getTime();
		 return formatter.format(incrementedDate);
	}

	private static Date getDateObject(String strDate){
		Date dateObject = null;
		try {
			dateObject = formatter.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateObject;
	}

	private static List<Tweet> removeDatesFromTweets(List<Tweet> allTweetsList, String[] datesToRemove){
		
		Iterator<Tweet> tweetIterator = allTweetsList.iterator();

        while(tweetIterator.hasNext()){
			Tweet tweet = tweetIterator.next();
			Date tweetDate = tweet.getDate();
			String strTweetDate = formatter.format(tweetDate);

			for (String dateToRemove : datesToRemove) {
				if(strTweetDate.compareTo(dateToRemove)==0){
					tweetIterator.remove();
				}	
			}
        }

		System.out.println("\n########## Total tweets after removing the tweets for some dates: " + allTweetsList.size() + "########");
		return allTweetsList; 
	}

}