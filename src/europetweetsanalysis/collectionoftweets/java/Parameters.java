package europetweetsanalysis.collectionoftweets.java;

public class Parameters {
    //================================= PATH PARAMETERS ============================================
    //--------------RESOURCES FOLDER---------------//
    static final String RESOURCES_PATH = "./resources/Europe_tweets_analysis/";
    static final String RESOURCES_PATH_COLLECTION_OF_TWEETS = RESOURCES_PATH + "Collection_of_tweets/";
    static final String KEYWORDS_FILE = RESOURCES_PATH_COLLECTION_OF_TWEETS + "keywords.txt";
	//---------------OUT FOLDER--------------//
    static final String OUT_PATH = "./out/Europe_tweets_analysis/";
    static final String OUT_PATH_COLLECTION_OF_TWEETS = OUT_PATH + "Collection_of_tweets/";
    static final String RAW_DATA_FILE = OUT_PATH_COLLECTION_OF_TWEETS + "extracted_tweets/raw_tweet_data";
    final static String ARFF_EXTRACTED_TWEET_FILE = "parsed_tweets/parsed_tweet_data.arff";
    //---------------SCRIPT AWK--------------//
    static final String AWK_FOLDER_PATH = "./src/europetweetsanalysis/collectionoftweets/awk/";
    static final String AWK_SCRIPT = AWK_FOLDER_PATH + "parse_raw_tweet.awk";
    //===============================================================================================

    //======================================= TWEET EXTRACTION ======================================
    static final String INITIAL_DATE = "2019-01-01";
    static final String FINAL_DATE = "2019-06-26";
    static final int NUMBER_OF_DAYS = 1;    //It's the length of sub date interval
    static final int MAX_TWEETS = -1;        //For all tweets set to -1
    static final String TWEETS_LANGUAGE = "it";     //For all languages set to "all"

    /*Hours to add to tweets date to have the date in italy time zone. 
    The time in json object returned by twitter is relative to PST time zone*/
    static final int HOUR_TO_ADD = -9;  
    //================================================================================================
}