package europetweetsanalysis.collectionoftweets.java;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Class to execute the web scraping of the twitter advanced search page using the criteria in a twitter criteria object
 * 
 * 
 * @author Jefferson Henrique
 * 
 * We have modified this class for our purposes (eg extract verified user field, set tweet language, 
 * convert date to italian time zone ecc)
 */
public class TweetDownloader {

	private static final HttpClient defaultHttpClient = HttpClients.custom()
			.setDefaultRequestConfig(RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()).build();

	static {
		Logger.getLogger("org.apache.http").setLevel(Level.OFF);
	}

	/** Get the list of tweets that specify the search criteria */
	public static List<Tweet> getTweets(TwitterCriteria criteria) {
		List<Tweet> results = new ArrayList<Tweet>();

		try {
			String refreshCursor = null;
			outerLace: while (true) {
				JSONObject json = new JSONObject(getURLResponse(criteria.getUsername(), criteria.getSince(),
						criteria.getUntil(), criteria.getQuerySearch(), criteria.getLanguage(), refreshCursor));

				refreshCursor = json.getString("min_position");

				Document doc = Jsoup.parse((String) json.get("items_html"));
				Elements tweets = doc.select("div.js-stream-tweet");

				if (tweets.size() == 0) {
					break;
				}

				for (Element tweet : tweets) {
					String usernameTweet = tweet.attr("data-screen-name");
					
					String txt = tweet.select("p.js-tweet-text").text().replaceAll("[^\\u0000-\\uFFFF]", "");

					int retweets = 0;
					String strRetweets = tweet.select("span.ProfileTweet-action--retweet span.ProfileTweet-actionCount")
							.attr("data-tweet-stat-count").replaceAll(",", "");
					if (isNumeric(strRetweets, "int")) {
						retweets = Integer.valueOf(strRetweets);
					}

					int favorites = 0;
					String strFavorites = tweet
							.select("span.ProfileTweet-action--favorite span.ProfileTweet-actionCount")
							.attr("data-tweet-stat-count").replaceAll(",", "");
					if (isNumeric(strFavorites, "int")) {
						favorites = Integer.valueOf(strFavorites);
					}

					String id = tweet.attr("data-tweet-id");

					String permalink = tweet.attr("data-permalink-path");

					Elements verifiedIcon = tweet.select("span.FullNameGroup span.UserBadges span.Icon--verified");

					String verified;
					if(verifiedIcon.first() == null){
						verified = "false";
						//System.out.println("NOT VERIFIED "+ permalink+" "+txt);
					}
					else{
						verified = "true";
						//System.out.println("VERIFIED " + permalink +" "+ usernameTweet +" " + verifiedIcon.first().text());

					}

					String geo = "";
					Elements geoElement = tweet.select("span.Tweet-geo");
					if (geoElement.size() > 0) {
						geo = geoElement.attr("title");
					}

					long dateMs = 0;
					String strDateMs = tweet.select("small.time span.js-short-timestamp").attr("data-time-ms");
					if (isNumeric(strDateMs, "long")) {
						dateMs = Long.valueOf(strDateMs);
					}

					if (dateMs != 0) {
						Date PSTDate = new Date(dateMs);
						Date date = addHourToDate(PSTDate, Parameters.HOUR_TO_ADD);
						//System.out.println(permalink +" "+ strDateMs + "  " + date.toString());

						Tweet t = new Tweet();
						t.setId(id);
						t.setPermalink("https://twitter.com" + permalink);
						t.setUsername(usernameTweet);
						t.setText(txt);
						t.setDate(date);
						t.setRetweets(retweets);
						t.setFavorites(favorites);
						t.setMentions(processTerms("(@\\w*)", txt));
						t.setHashtags(processTerms("(#\\w*)", txt));
						t.setGeo(geo);
						t.setVerified(verified);

						results.add(t);
					}

					if (criteria.getMaxTweets() > 0 && results.size() >= criteria.getMaxTweets()) {
						break outerLace;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * The function return a JSON response used by Twitter to build its results
	 */
	private static String getURLResponse(String username, String since, String until, String querySearch,
			String language, String scrollCursor) throws Exception {
		String appendQuery = "";
		if (username != null) {
			appendQuery += "from:" + username;
		}
		if (since != null) {
			appendQuery += " since:" + since;
		}
		if (until != null) {
			appendQuery += " until:" + until;
		}
		if (querySearch != null) {
			appendQuery += " " + querySearch;
		}

		String url = String.format(
				"https://twitter.com/i/search/timeline?f=realtime&l=%s&q=%s&src=typd&max_position=%s", language,
				URLEncoder.encode(appendQuery, "UTF-8"), scrollCursor);
		//System.out.println(url);
		HttpGet httpGet = new HttpGet(url);
		HttpEntity resp = defaultHttpClient.execute(httpGet).getEntity();
		return EntityUtils.toString(resp);
	}

	private static String processTerms(String patternS, String tweetText) {
		StringBuilder sb = new StringBuilder();
		Matcher matcher = Pattern.compile(patternS).matcher(tweetText);
		while (matcher.find()) {
			sb.append(matcher.group());
			sb.append(" ");
		}

		return sb.toString().trim();
	}

	private static boolean isNumeric(String strNum, String format) {
		try {
			switch (format) {
			case "int":
				int intNum = Integer.parseInt(strNum);
				break;

			case "long":
				long longNum = Long.parseLong(strNum);
				break;

			default:
				break;
			}
		} catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}

	private static Date addHourToDate(Date PSTDate, int hoursToAdd){
		Calendar c = Calendar.getInstance();
		c.setTime(PSTDate);
        c.add(Calendar.HOUR_OF_DAY, hoursToAdd);

        return c.getTime();
	}

}