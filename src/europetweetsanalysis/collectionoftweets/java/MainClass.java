package europetweetsanalysis.collectionoftweets.java;

import europetweetsanalysis.utility.java.*;

import java.util.ArrayList;

import java.util.List;

/**
 * Download tweets and save in a file
 */
public class MainClass {
	public static void main(String[] args) {
		List<Tweet> tweetList;

		ArrayList<String> keywordsList = FileManager.readStringsListFile(Parameters.KEYWORDS_FILE);

		tweetList = TweetQueryManager.getTweetsByKeywords(keywordsList, Parameters.INITIAL_DATE, Parameters.FINAL_DATE, Parameters.TWEETS_LANGUAGE, Parameters.MAX_TWEETS);
		
		TweetStore tweetStore = new TweetStore(Parameters.RAW_DATA_FILE);
		tweetStore.storeTweetsWithoutDuplicate(tweetList);
	}

}
