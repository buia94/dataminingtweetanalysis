package europetweetsanalysis.collectionoftweets.java;

import europetweetsanalysis.utility.java.*;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.HashSet;

public class TweetStore {

	private String outputFilePath;
	private final SimpleDateFormat dataFormat;

	public TweetStore(String outputFilePath) {
		this.outputFilePath = outputFilePath;
		dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
	}

	/**
	 * Store all collected tweets in an output file without duplicate tweets
	 */
	public void storeTweetsWithoutDuplicate(List<Tweet> tweetList) {
		System.out.println("\nStart. Print tweet to file \"" + outputFilePath + "\".");

		//outputFilePath = FileManager.getNewFilePath(outputFilePath);	// Use this function to don't overwrite another files with same name

		HashSet<String> idTweetList = new HashSet<String>(); // Is used to store the tweet id and exclude the duplicates
		StringBuilder stringToStore = new StringBuilder("");
 
		for (Tweet t : tweetList) {
			String tweetId = t.getId();
			
			if (!idTweetList.contains(tweetId)) {
				idTweetList.add(tweetId);

				stringToStore.append(String.format("%s,\n%s,\n%s,\n%s,\n%d,\n%s,\n%s,\n%d,\n%s,\n%s,\n%s,\n\n", t.getId(),
				dataFormat.format(t.getDate()), t.getHashtags(), t.getText(), t.getRetweets(),
				t.getGeo(), t.getMentions(), t.getFavorites(), t.getUsername(), t.getVerified(), t.getPermalink()));
				
			}
		}
		FileManager.printStringToFile(stringToStore.toString(), outputFilePath, false);
		
		System.out.println("\nDone. Output file generated \"" + outputFilePath + "\".");
		System.out.println("\n########## Total tweets without duplicate are: " + idTweetList.size() + "############");
	}

}
