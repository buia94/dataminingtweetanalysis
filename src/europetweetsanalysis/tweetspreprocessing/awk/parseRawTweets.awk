#awk -v out_file=[output_file_name].arff -f [awk_script] [path_file]/raw_tweet_data
#
#The script only keeps the selected fields and prints the file in the same format.
#
#The fields maintained are tweet text, tweet id, username, date, verified
#
#The class field has also been added (question mark in this case, because it is not known).
#

BEGIN{
    FS=",\n"
    RS="\n\n"
    print "Parse raw_tweet: start\n"
}

{
    tweetText = $4
    tweetId = $1
    username = $9
    date = $2
    verified = $10
    printf "%s\n%s\n%s\n%s\n%s\n?\n\n", tweetText, username, date, tweetId, verified > out_file
}

END{ 
    print "Parse raw_tweet: end"
}