#awk -v out_file=[output_file_name].arff -f [awk_script] [path_file]/raw_tweet_data
#
#The script remove links, mentions, numbers and special characters (e.g., punctuation marks, brackets, slashes, quotes, etc.). Hashtags are not
#completely discarded, they are instead reduced to word. All characters are converted to the lower case form. 
#

BEGIN{
    FS="\n"
    RS="\n\n"
    itemsToHold[1] = 1; #text
    itemsToHold[2] = 2; #username
    itemsToHold[3] = 3; #date
    itemsToHold[4] = 4; #tweetId
    itemsToHold[5] = 5; #verified
    itemsToHold[6] = 6; #class
    
    print "\nParse raw_tweet: start\n"
    print  "@relation tweets\n" > out_file
    print "@attribute Text string" > out_file
    print "@attribute Username string" > out_file
    print "@attribute Date string" > out_file
    print "@attribute Id string" > out_file
    print "@attribute Verified {true, false}" > out_file
    print "@attribute Class {pro, contro, neutro}" > out_file
    print "\n@data\n" > out_file
}

{
    for(i in itemsToHold){
        if(i==1){
            tweetText = $itemsToHold[i]

            #remove url
            filteredtextByUrl = gensub(/https?(:\/\/(www\.)?[[:alnum:]\?@:%\.\/\-_\+~\#=]{2,256})?/,"", "g", tweetText)
                
            #remove pic.twitter link
            filteredtextByPic = gensub(/pic\.twitter[[:alnum:]@:%\.\/\-_\+~\#=]*/,"", "g", filteredtextByUrl)

            #remove tag
            filteredtextByTag = gensub(/@[[:alnum:]@:%\.\/\-_\+~\#=]*/,"", "g", filteredtextByPic)  

            #remove special characters and hashtag symbols
            filteredtextBySpecialChar = gensub(/[\#[:punct:]]/," ", "g", filteredtextByTag) 

            #remove numbers
            filteredByNumbers = gensub(/[[:digit:]]/," ", "g", filteredtextBySpecialChar) 

            #remove extra spaces
            filteredByExtraSpaces = gensub(/[[:space:]]+/," ", "g", filteredByNumbers)

            #convert all words to lower case
            printf "'"tolower(filteredByExtraSpaces)"'" > out_file
            }
        
        #print the other items to hold
        else{
            
            #print the apexes for the string attributes
            if(i<=4){
                printf "'"$itemsToHold[i]"'" > out_file
            }
            else{
                printf $itemsToHold[i] > out_file
            }
        }

        if(i<length(itemsToHold)){
            #print the comma between the fields
            printf "," > out_file
        }

    }
    #print a new line between the rows
    print "" > out_file 
 }

END{ 
    print "Parse raw_tweet: end"
}