#awk -v out_file=[output_file_name].arff -f [awk_script] [path_file]/file1 [path_file]/file2 ...
#
#The script merge several file removing duplicate rows
#
BEGIN{
    print "\n-------Start merging--------\n"
}

{
    if (!a[$0]++){
        print $0 > out_file
    } 
}

END{ 
    print "\n-------End merging--------\n"
}
