package europetweetsanalysis.tweetspreprocessing.java;

import europetweetsanalysis.utility.java.*;

import java.util.ArrayList;
import java.util.Collections;

public class PreprocessingUtils {

	/**
	 * Read the raw tweets file and parse it using an awk script. The result is
	 * stored in another file
	 */
	public static void parseAndStoreTweets(String rawTweetsFilePath, String outputFilePath) {
		System.out.println("\n------- Start parsing raw tweets -----\n");
		String command = "awk -v out_file=" + outputFilePath + " -f " + Parameters.PARSE_RAW_TWEETS_SCRIPT + " "
				+ rawTweetsFilePath;
		boolean printErrorStream = true;
		String outputCommand = BashExecutor.executeScript(command, printErrorStream);
		System.out.println(outputCommand);
		System.out.println("\n------- End parsing ----\n-");
	}

	/**
	 * Read the parsed tweets file and return it structured in an arraylist of
	 * string array. The fieldsNumber is summed by 1 because there is a new row
	 * between the tweets
	 */
	public static ArrayList<String[]> getStructuredParsedTweets(String parsedTweetsFilePath, int fieldsNumber) {
		ArrayList<String> rowsParsedTweetsFile = FileManager.readStringsListFile(parsedTweetsFilePath);
		ArrayList<String[]> structuredParsedTweets = new ArrayList<String[]>();
		for (int i = 0; i < rowsParsedTweetsFile.size(); i = i + fieldsNumber + 1) {
			String[] tweetInformations = new String[fieldsNumber];
			for (int j = 0; j < fieldsNumber; j++) {
				tweetInformations[j] = rowsParsedTweetsFile.get(i + j);
			}
			structuredParsedTweets.add(tweetInformations);
		}
		return structuredParsedTweets;
	}

	/**
	 *  Keep the structured parsed tweets and shuffle it 
	 */
	public static ArrayList<String[]> getShuffledTweets(ArrayList<String[]> toShuffleTweets) {
		Collections.shuffle(toShuffleTweets);
		return toShuffleTweets;
	}

	/**
	 * Return a sample of tweets according to first e last element given as
	 * parameters
	 */
	public static ArrayList<String[]> getSampledTweets(ArrayList<String[]> tweetsToSample, int firstElement,
			int lastElement) {
		ArrayList<String[]> sampledTweets = null;
		if (lastElement > tweetsToSample.size()) {
			throw new RuntimeException("Last element value is higher than arraylist size");
		}
		sampledTweets = new ArrayList<String[]>(tweetsToSample.subList(firstElement, lastElement));
		return sampledTweets;
	}

	/**
	 * Call the getSampledTweets function with last element equal to tweetsToSample
	 * size
	 */
	public static ArrayList<String[]> getSampledTweets(ArrayList<String[]> tweetsToSample, int firstElement) {
		return getSampledTweets(tweetsToSample, firstElement, tweetsToSample.size());
	}

	/**
	 *  Store structured tweets in an output file 
	 */
	public static void storeTweetsToFile(ArrayList<String[]> tweetsToStore, String outputFilePath) {
		StringBuilder allRandomTweets = new StringBuilder("");
		for (int i = 0; i < tweetsToStore.size(); i++) {
			String[] tweetInformations = tweetsToStore.get(i);
			for (int j = 0; j < tweetInformations.length; j++) {
				allRandomTweets.append(tweetInformations[j] + "\n");
			}

			if(i%1000==0){
				System.out.println("Tweet number: " + i + " of " + tweetsToStore.size());
			}
			allRandomTweets.append("\n");
		}
		//outputFilePath = FileManager.getNewFilePath(outputFilePath);	//Use this function to don't overwrite another files with same name
		boolean append = false;
		FileManager.printStringToFile(allRandomTweets.toString(), outputFilePath, append);
	}

	/**
	 * Call the cleanAndStoreTweets function for all the tweets in the sampled
	 * tweets folder and save them in the cleaned tweets folder
	 */
	public static void cleanAndStoreAllTweets(String sampledTweetsFolder, String cleanedTweetsFolder) {
		String[] sampledFilesNames = FileManager.readAllFileName(sampledTweetsFolder);
		for (String sampledFileName : sampledFilesNames) {
			cleanAndStoreTweets(sampledTweetsFolder + sampledFileName, cleanedTweetsFolder + sampledFileName + ".arff");
		}
	}

	/**
	 * Read the sampled tweets and clean it using an awk script. The result is
	 * stored in an output file in the arff format
	 */
	public static void cleanAndStoreTweets(String sampledTweetsFilePath, String outputFilePath) {
		String command = "awk -v out_file=" + outputFilePath + " -f " + Parameters.CLEAN_TWEETS_SCRIPT + " "
				+ sampledTweetsFilePath;
		System.out.println(command);
		boolean printErrorStream = true;
		String outputCommand = BashExecutor.executeScript(command, printErrorStream);
		System.out.println(outputCommand);
	}

	/**
	 * Merge all the tweets file contained in an input folder, using an awk script
	 * and save the merged tweets file in the same folder of the merged tweets,
	 * without duplicate tweets
	 */
	public static void mergingAndSaveTweets(String inputFolder) {
		String mergedTweetsFile = inputFolder + Parameters.MERGED_INSTANCES + ".arff";
		String pattern = ".*\\.arff";
		String[] arffFilesNames = FileManager.readAllFileName(inputFolder, pattern);
		String command = "awk -v out_file=" + mergedTweetsFile + " -f " + Parameters.MERGE_FILES_SCRIPT;
		for (String arffFile : arffFilesNames) {
			command += " " + inputFolder + arffFile;
		}
		boolean printErrorStream = false;
		String outputCommand = BashExecutor.executeScript(command, printErrorStream);
		System.out.println(outputCommand);
	}
}