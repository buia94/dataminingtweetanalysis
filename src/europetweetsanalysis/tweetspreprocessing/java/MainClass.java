package europetweetsanalysis.tweetspreprocessing.java;

import java.util.ArrayList;

//import europetweetsanalysis.utility.java.FileManager;

public class MainClass {
	/** 
	 * The main performs the operation passed as a parameter (eg parse tweets)
	 * It is used to do all the preprocessing steps
	*/
	public static void main(String[] args) {
		String operationToExecute = args[0];
		String parsedTweetsFilePath = Parameters.OUT_PATH_PARSED_TWEETS + Parameters.PARSED_TWEETS_FILE;
		String labeledSampledTweetsFilePath = Parameters.MANUALLY_LABELED_SAMPLED_TWEETS + Parameters.LABELED_DATA_FILE;
		String notLabeledSampledTweetsFilePath = Parameters.NOT_LABELED_SAMPLED_TWEETS + Parameters.NOT_LABELED_DATA_FILE;
		switch (operationToExecute) {
			case "parseTweets":
				//parsedTweetsFilePath = FileManager.getNewFilePath(parsedTweetsFilePath); // Use this function to don't overwrite another files with same name
				String rawDataPath = Parameters.OUT_PATH_COLLECTION_OF_TWEETS + Parameters.RAW_DATA_FILE;
				PreprocessingUtils.parseAndStoreTweets(rawDataPath, parsedTweetsFilePath);
				break;
			case "sampleTweetsDataset":
				ArrayList<String[]> structuredParsedTweets = PreprocessingUtils.getStructuredParsedTweets(parsedTweetsFilePath, Parameters.TWEET_FIELDS_NUMBER);
				ArrayList<String[]> shuffledTweets = PreprocessingUtils.getShuffledTweets(structuredParsedTweets);
				
				if(Parameters.TWEETS_TO_LABEL_NUMBER!=0){
					storeTweetsSampleToFile(shuffledTweets, labeledSampledTweetsFilePath, 0, Parameters.TWEETS_TO_LABEL_NUMBER);
				}
				storeTweetsSampleToFile(shuffledTweets, notLabeledSampledTweetsFilePath, Parameters.TWEETS_TO_LABEL_NUMBER);
				break;
			case "cleanAllTweets":
				if(Parameters.TWEETS_TO_LABEL_NUMBER!=0){
					PreprocessingUtils.cleanAndStoreAllTweets(Parameters.MANUALLY_LABELED_SAMPLED_TWEETS, Parameters.MANUALLY_LABELED_CLEANED_TWEETS);
				}
				PreprocessingUtils.cleanAndStoreAllTweets(Parameters.NOT_LABELED_SAMPLED_TWEETS, Parameters.NOT_LABELED_CLEANED_TWEETS);
				break;
			case "mergingLabeledTweets":
				PreprocessingUtils.mergingAndSaveTweets(Parameters.MANUALLY_LABELED_CLEANED_TWEETS);
				break;
			default:
				break;
		}
	}

	private static void storeTweetsSampleToFile(ArrayList<String[]> shuffledTweets, String outputFile, int firstElement, int lastElement){
		ArrayList<String[]> sampledTweets = PreprocessingUtils.getSampledTweets(shuffledTweets, firstElement, lastElement);
		PreprocessingUtils.storeTweetsToFile(sampledTweets, outputFile);
	}
	
	private static void storeTweetsSampleToFile(ArrayList<String[]> shuffledTweets, String outputFile, int firstElement){
		ArrayList<String[]> sampledTweets = PreprocessingUtils.getSampledTweets(shuffledTweets, firstElement);
		PreprocessingUtils.storeTweetsToFile(sampledTweets, outputFile);
	}
}
