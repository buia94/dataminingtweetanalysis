package europetweetsanalysis.tweetspreprocessing.java;

public class Parameters {

	//================================= PATH PARAMETERS ============================================
	//---------------OUT FOLDER--------------//
	final static String OUT_PATH = "./out/Europe_tweets_analysis/";
	static final String OUT_PATH_COLLECTION_OF_TWEETS = OUT_PATH + "Collection_of_tweets/";	
	static final String OUT_PATH_TWEETS_PREPROCESSING = OUT_PATH + "Tweets_preprocessing/";	

	static final String OUT_PATH_SAMPLED_TWEETS = OUT_PATH_TWEETS_PREPROCESSING + "sampled_tweets/";	
	static final String OUT_PATH_CLEANED_TWEETS = OUT_PATH_TWEETS_PREPROCESSING + "cleaned_tweets/";	
	static final String OUT_PATH_PARSED_TWEETS = OUT_PATH_TWEETS_PREPROCESSING + "parsed_tweets/";

	final static String MANUALLY_LABELED_SAMPLED_TWEETS = OUT_PATH_SAMPLED_TWEETS + "manually_labeled_tweets/";
	final static String NOT_LABELED_SAMPLED_TWEETS = OUT_PATH_SAMPLED_TWEETS + "not_labeled_tweets/";
	final static String MANUALLY_LABELED_CLEANED_TWEETS = OUT_PATH_CLEANED_TWEETS + "manually_labeled_tweets/";
	final static String NOT_LABELED_CLEANED_TWEETS = OUT_PATH_CLEANED_TWEETS + "not_labeled_tweets/";
	
	final static String RAW_DATA_FILE = "extracted_tweets/raw_tweet_data";

	final static String PARSED_TWEETS_FILE = "parsed_tweet_data";
	final static String LABELED_DATA_FILE = "manually_labeled_tweet";
	final static String NOT_LABELED_DATA_FILE = "not_labeled_tweet";
	final static String MERGED_INSTANCES = "labeled_merged_instances";
	
	//---------------SCRIPT AWK--------------//
	static final String AWK_FOLDER_PATH = "./src/europetweetsanalysis/tweetspreprocessing/awk/";

	static final String PARSE_RAW_TWEETS_SCRIPT = AWK_FOLDER_PATH + "parseRawTweets.awk";
	static final String CLEAN_TWEETS_SCRIPT = AWK_FOLDER_PATH + "cleanTweets.awk";
	static final String MERGE_FILES_SCRIPT = AWK_FOLDER_PATH + "mergeFilesWithoutDuplicate.awk";
	//===============================================================================================


    //-------------------------RANDOM DATASET SAMPLING PARAMETERS-------------------------------
	final static int SEED = 10;	//seed used to shuffle tweets

	final static int TWEET_FIELDS_NUMBER = 6;	//The tweet fields number is used in the getStructuredParsedTweetsRead() to structure in an arraylist of string array the parsed tweets file and execute the shuffle on it
	final static int TWEETS_TO_LABEL_NUMBER = 0;	//Number of tweets to sample that I want to label manually
    //--------------------------------------------------------------------------------

}