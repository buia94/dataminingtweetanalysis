package europetweetsanalysis.tweetsanalysis.peakanalysis.java;

import europetweetsanalysis.utility.java.BashExecutor;
import europetweetsanalysis.utility.java.FileManager;

import java.util.TreeMap;

import weka.core.Instances;


public class MainClass {
    /**
	 * The main performs the peak analysis collecting information on the number of tweets for each single day. 
     * The classes counter for each day are stored in a file.
     * We can plot the tweet counter using a matlab script
	 */
    public static void main(String[] args) {
        String operationToExecute = args[0];

        switch (operationToExecute) {
            case "storeTweetCounter":
                storeTweetCounter();
                break;
            case "plotTweetCounter":
                plotTweetCounter();
                break;
            default:
                break;
            }
    }

    private static void storeTweetCounter(){
        String inputFilePath = Parameters.CLASSIFIED_TWEETS_FOLDER + Parameters.ARFF_CLASSIFIED_TWEETS_FILE;
        String outputFilePath = Parameters.OUT_PATH_DATE_CLASS_COUNTER + Parameters.DATE_CLASS_COUNTER_FILE;
        String classesValues[] = Parameters.CLASSES_VALUES;

        Instances trainingSet = FileManager.readInstancesFromArff(inputFilePath);

        TweetsCounterByDate tweetsCounterByDate = new TweetsCounterByDate(trainingSet, Parameters.GET_VERIFIED_USERS, classesValues);
        TreeMap<String, int[]> tweetsCountbyDate = tweetsCounterByDate.getTweetsCountByDate();

        TweetsCounterByDateStore tweetsCounterByDateStore = new TweetsCounterByDateStore(outputFilePath);
        tweetsCounterByDateStore.storeTweetsCountbyDate(tweetsCountbyDate, classesValues);
    }

    private static void plotTweetCounter(){
        String command = "matlab -nodisplay -nosplash -nodesktop -r run('"+Parameters.PLOT_TWEETS_COUNTER_SCRIPT+"');exit;";
        
		boolean printErrorStream = true;
		String outputCommand = BashExecutor.executeScript(command, printErrorStream);
		System.out.println(outputCommand);
    }
}