package europetweetsanalysis.tweetsanalysis.peakanalysis.java;

import java.util.TreeMap;

import weka.core.Instance;
import weka.core.Instances;

public class TweetsCounterByDate {
    TreeMap<String, int[]> tweetsCountbyDate;
    private Instances trainingSet;
    private String[] classesValues;

    /**
     * Receive the training set with all tweets and compute number of tweets in each date 
    */
    public TweetsCounterByDate(Instances trainingSet, Boolean keepVerifiedUsers, String[] classesValues) {
        this.trainingSet = trainingSet;
        tweetsCountbyDate = new TreeMap<String, int[]>(); // we use a tree map because the update costs less and the insertion is ordered
        this.classesValues = classesValues;
        computeTweetsCounter(keepVerifiedUsers);
    }

    /**
     * Return the structure with the number of tweet for each date 
    */
    public TreeMap<String, int[]> getTweetsCountByDate(){
        return tweetsCountbyDate;
    }

    /**
     * Compute the tweets count for each date 
    */
    private void computeTweetsCounter(Boolean keepVerifiedUsers) {
        for (int i = 0; i < trainingSet.numInstances(); i++) {
            Instance instance = trainingSet.get(i);

            boolean verified = false;
            if (instance.stringValue(Parameters.VERIFIED_INDEX).compareTo("true") == 0) {
                verified = true;
            }

            if (keepVerifiedUsers == null || verified == keepVerifiedUsers.booleanValue()) {
                String date = instance.stringValue(Parameters.DATE_INDEX);
                String dateWithoutHour = date.split(" ")[0];

                if (instance.stringValue(Parameters.VERIFIED_INDEX).compareTo("true") == 0) {
                    verified = true;
                }

                String classValue = instance.stringValue(instance.numAttributes() - 1);

                updateTweetsCounter(dateWithoutHour, classValue);
            }
        }
    }

    /**
     * Update the tweets count for an input date and for an input class value 
     */
    private void updateTweetsCounter(String dateWithoutHour, String classValue) {
        if (tweetsCountbyDate.containsKey(dateWithoutHour)) {
            for (int j = 0; j < classesValues.length; j++) {
                if (classValue.compareTo(classesValues[j]) == 0) {
                    int[] tweetsCounter = tweetsCountbyDate.get(dateWithoutHour);
                    tweetsCounter[j]++;
                    tweetsCountbyDate.put(dateWithoutHour, tweetsCounter);
                }
            }
        } 
        else {
            tweetsCountbyDate.put(dateWithoutHour, new int[classesValues.length]);
        }
    }

}