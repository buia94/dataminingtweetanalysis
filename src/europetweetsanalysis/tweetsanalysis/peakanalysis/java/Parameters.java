package europetweetsanalysis.tweetsanalysis.peakanalysis.java;

public class Parameters {

    //================================= PATH PARAMETERS ============================================
	final static String OUT_PATH = "./out/Europe_tweets_analysis/";
	final static String OUT_PATH_CLASSIFICATION_OF_TWEETS = OUT_PATH + "Classification_of_tweets/";
	final static String OUT_PATH_PEAK_ANALYSIS = OUT_PATH + "Tweets_analysis/Peak_Analysis/";
	final static String OUT_PATH_DATE_CLASS_COUNTER = OUT_PATH_PEAK_ANALYSIS + "date_class_counter/";

	final static String CLASSIFIED_TWEETS_FOLDER = OUT_PATH_CLASSIFICATION_OF_TWEETS + "classified_tweets/";

	final static String ARFF_CLASSIFIED_TWEETS_FILE = "0_classified_tweet.arff";
	final static String DATE_CLASS_COUNTER_FILE = "date_class_counter.csv";

	//---------------SCRIPT MATLAB--------------//
	static final String MATLAB_FOLDER_PATH = "./src/europetweetsanalysis/tweetsanalysis/peakanalysis/matlab/";

	static final String PLOT_TWEETS_COUNTER_SCRIPT = MATLAB_FOLDER_PATH + "plotTweetCounter.m";
	//===============================================================================================

	//================================= PEAK MANAGER PARAMETERS =======================
	final static int DATE_INDEX = 2;
	final static int VERIFIED_INDEX = 4;
	final static Boolean GET_VERIFIED_USERS = null;	//To don't take verified users set to false. To take all users set to null

	final static String[] CLASSES_VALUES = {"pro", "contro", "neutro"};
	//===============================================================================================

}