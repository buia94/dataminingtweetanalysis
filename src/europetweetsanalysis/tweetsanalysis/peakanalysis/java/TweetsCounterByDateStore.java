package europetweetsanalysis.tweetsanalysis.peakanalysis.java;

import java.util.TreeMap;

import europetweetsanalysis.utility.java.FileManager;

import java.util.Map;

public class TweetsCounterByDateStore {
    private String outputFilePath;

    public TweetsCounterByDateStore(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    /**
     * Store in a file the number of tweets in each date for each class
     */
    public void storeTweetsCountbyDate(TreeMap<String, int[]> tweetsCountbyDate, String[] classesValues) {
        StringBuilder stringToStore = new StringBuilder("");
        stringToStore.append("Date, ");
        for (int i = 0; i < classesValues.length; i++) {
            if(i< classesValues.length - 1){
                stringToStore.append(classesValues[i] + ", ");
            }
            else{
                stringToStore.append(classesValues[i] + "\n");
            }
        }

        for (Map.Entry<String, int[]> dateMapEntry : tweetsCountbyDate.entrySet()) {
            stringToStore.append(dateMapEntry.getKey() + ", ");
            int[] tweetsCounter = dateMapEntry.getValue();
            for (int i = 0; i < tweetsCounter.length; i++) {
                if(i < tweetsCounter.length - 1){
                    stringToStore.append(tweetsCounter[i] + ", ");
                }
                else{
                    stringToStore.append(tweetsCounter[i]);
                }
            }
            stringToStore.append("\n");
        }

        FileManager.printStringToFile(stringToStore.toString(), outputFilePath, false);
    }
}