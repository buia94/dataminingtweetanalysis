%the script read the file with the number of tweet for each class (pro
%contro neutro) for a specific date and plot the tree curves over the time in order to
%show peaks of the number of tweets 

dateclasscounter=readtable("../../../../../out/Europe_tweets_analysis/Tweets_analysis/Peak_Analysis/date_class_counter/final_date_class_counter.csv","Delimiter",',','DateLocale','it_IT');
dates=[];

for i=1:numel(dateclasscounter.Date)
   dates=[dates,datetime(dateclasscounter.Date(i),'InputFormat','yyyy/MM/dd')];
end

pro=dateclasscounter.pro;
contro=dateclasscounter.contro;
neutro=dateclasscounter.neutro;

fig=figure;
set(fig, 'Visible', 'off');
hold on;
plot(dates,contro);
plot(dates,pro);
plot(dates,neutro);
ylabel('Tweet Number');
legend({'contro','pro','neutro'});
saveas(fig,"../../../../../out/Europe_tweets_analysis/Tweets_analysis/Peak_Analysis/date_tweetsnumber_graphs/peaks.fig");
saveas(fig,"../../../../../out/Europe_tweets_analysis/Tweets_analysis/Peak_Analysis/date_tweetsnumber_graphs/peaks.png");
