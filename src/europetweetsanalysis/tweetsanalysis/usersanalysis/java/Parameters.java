package europetweetsanalysis.tweetsanalysis.usersanalysis.java;

public class Parameters {

    //================================= PATH PARAMETERS ============================================
	final static String OUT_PATH = "./out/Europe_tweets_analysis/";
	final static String OUT_PATH_CLASSIFICATION_OF_TWEETS = OUT_PATH + "Classification_of_tweets/";
	final static String OUT_PATH_USER_ANALYSIS = OUT_PATH + "Tweets_analysis/Users_Analysis/";
	final static String OUT_PATH_USER_CLASS_COUNTER = OUT_PATH_USER_ANALYSIS + "users_class_counter/";

	final static String CLASSIFIED_TWEETS_FOLDER = OUT_PATH_CLASSIFICATION_OF_TWEETS + "classified_tweets/";

	final static String ARFF_CLASSIFIED_TWEETS_FILE = "0_classified_tweet.arff";
	final static String USERS_CLASS_COUNTER_FILE = "users_class_counter.txt";

	//===============================================================================================

	 //================================= USER INFORMATIONS MANAGER PARAMETERS =======================
	 final static int USERNAME_INDEX = 1;
	 final static int VERIFIED_INDEX = 4;
	 final static Boolean GET_VERIFIED_USERS = null;	//To don't take verified users set to false. To take all users set to null
	 //===============================================================================================

	 //================================= USER PARAMETERS===============================================
	 final static String[] CLASSES_VALUES = {"pro", "contro", "neutro"};
	 final static double USER_THRESHOLD = 0.2;
	 //===============================================================================================

}