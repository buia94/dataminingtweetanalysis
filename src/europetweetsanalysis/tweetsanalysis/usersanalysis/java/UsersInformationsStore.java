package europetweetsanalysis.tweetsanalysis.usersanalysis.java;

import java.util.ArrayList;

import europetweetsanalysis.utility.java.FileManager;

public class UsersInformationsStore {
    private String outputFilePath;

    public UsersInformationsStore(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    /*
     * Store in a file all the information about the users: the counting of tweets
     * for each possible class values (pro, contro, neutro), user score and
     * verification info
     */
    public void storeUsersInformation(ArrayList<User> usersInfo) {
        StringBuilder stringToStore = new StringBuilder("");
        for (User user : usersInfo) {
            int[] classesCounter = user.getClassesCounter();
            String[] classesValues = user.getClassesValue();
            String username = user.getUsername();

            stringToStore.append(username);
            stringToStore.append("\nTotal tweets: " + user.getTotalTweetsNumber() + "\n");
            for (int i = 0; i < classesValues.length; i++) {
                stringToStore.append(classesValues[i] + ": ");
                stringToStore.append(classesCounter[i] + " ");
            }
            stringToStore.append("\nScore: " + user.getUserScore());
            stringToStore.append("\nUser class: " + user.getUserClass(Parameters.USER_THRESHOLD));
            stringToStore.append("\nVerified: " + user.isVerified() + "\n\n");
        }
        FileManager.printStringToFile(stringToStore.toString(), outputFilePath, false);
    }
}