package europetweetsanalysis.tweetsanalysis.usersanalysis.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import weka.core.Instance;
import weka.core.Instances;

public class UsersInformationsManager {
    private HashMap<String, User> usersInfo;
    private Instances trainingSet;
    private String[] classesValues;

    /*Receive the training set with all tweets and extract users informations */
    public UsersInformationsManager(Instances trainingSet, String[] classesValues) {
        this.trainingSet = trainingSet;
        usersInfo = new HashMap<String, User>();    //we use an hash map because the update costs less
        this.classesValues = classesValues;
        computeUsersInfo();
    }

    /*
     * Return the list that cointain all the user object 
     */
    public ArrayList<User> getUsersInfo() {
        return new ArrayList<>(usersInfo.values());
    }

    /* Get the sorted usersInfo list by user total tweets number */ 
    public ArrayList<User> getSortedUsersInfo() 
    { 
        ArrayList<User> sortedUserInfoList = new ArrayList<>(usersInfo.values());
        Collections.sort(sortedUserInfoList);

        return sortedUserInfoList;
    } 

    /*
     * Get the users stance for verified, unverified or all users, in terms of
     * number of distinct users for single class
     */
    public int[] getUsersStance(double userThreshold, Boolean verified) {
        int[] usersStance = new int[classesValues.length];
        for (Map.Entry<String, User> userMapElement : usersInfo.entrySet()) {

            User user = (User) userMapElement.getValue();

            if ((verified == null)||(user.isVerified() == verified)) {
                String userClass = user.getUserClass(userThreshold);

                for (int i = 0; i < usersStance.length; i++) {
                    if (userClass.compareTo(classesValues[i]) == 0) {
                        usersStance[i]++;
                    }
                }
            }
        }
        return usersStance;
    }

    /*
     * Get the tweets stance for verified, unverified or all users, in terms of
     * number of tweets for single class
     */
    public int[] getTweetsStance(Boolean verified) {
        int[] tweetsStance = new int[classesValues.length];
        for (Map.Entry<String, User> userMapElement : usersInfo.entrySet()) {

            User user = (User) userMapElement.getValue();
            int[] userClassesCounter = user.getClassesCounter();
            if ((verified == null)||(user.isVerified() == verified)) {
                for (int i = 0; i < tweetsStance.length; i++) {
                    tweetsStance[i] += userClassesCounter[i];
                }
            }
        }
        return tweetsStance;
    }

    /*This return all users stance (verified and unverified)*/
    public int[] getUsersStance(double userThreshold) {
        return getUsersStance(userThreshold, null);
    }

    /*Read all the tweets in the training set and compute the usersInfo hashmap */
    private void computeUsersInfo() {
        for (int i = 0; i < trainingSet.numInstances(); i++) {
            Instance instance = trainingSet.get(i);

            String username = instance.stringValue(Parameters.USERNAME_INDEX);
            boolean verified = false;
            if (instance.stringValue(Parameters.VERIFIED_INDEX).compareTo("true")==0) {
                verified = true;
            }
            String classValue = instance.stringValue(instance.numAttributes() - 1);

            updateUsersInfo(username, verified, classValue);
        }
    }

    /* Update the users info structure for a specific username */
    private void updateUsersInfo(String username, boolean verified, String classValue) {
        User user = getUser(username, verified);
        user.updateClassesCounter(classValue);
        usersInfo.put(username, user);
    }

    /*
     * Return the user object containing information about the counting of tweets
     * for each possible class values and the information about verification
     */
    private User getUser(String username, boolean verified) {
        User user;
        if (usersInfo.containsKey(username)) {
            user = usersInfo.get(username);
        } else {
            user = new User(username, verified, classesValues);
        }
        return user;
    }
}