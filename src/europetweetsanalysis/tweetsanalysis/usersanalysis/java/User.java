package europetweetsanalysis.tweetsanalysis.usersanalysis.java;

public class User implements Comparable<User> {
    private String username;
    private boolean verified;
    private int[] classesCounter; // The classes are in order: pro, contro, neutro
    private String[] classesValues;

    /** Model class to store the user information */
    public User(String username, boolean verified, String[] classesValues) {
        this.username = username;
        this.verified = verified;
        this.classesValues = classesValues;
        classesCounter = new int[classesValues.length];
    }

    public void updateClassesCounter(String className) {
        for (int i = 0; i < classesValues.length; i++) {
            if (className.compareTo(classesValues[i]) == 0) {
                classesCounter[i]++;
            }
        }
    }

    /**
     * The user's score is used to understand what the user's orientation is,
     * based on the class counter
     */
    public double getUserScore() {
        int totalTweets = getTotalTweetsNumber();
        double userScore = 0.0;

        if (totalTweets != 0) {
            userScore = ((double) (classesCounter[0] - classesCounter[1]) / (double) totalTweets);
        }
        return userScore;
    }

    /**
     * Returns the user class based on the input threshold and user threshold
     */
    public String getUserClass(double threshold) {
        String userClass = "";
        double userScore = getUserScore();

        if (userScore > threshold) {
            userClass = classesValues[0]; // pro
        } else {
            if (userScore >= (-threshold)) {
                userClass = classesValues[2]; // neutro
            } else {
                userClass = classesValues[1]; // contro
            }
        }
        return userClass;
    }

    /**
     * Returns the username of the user
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the classes values (pro, contro, neutro)
     */
    public String[] getClassesValue() {
        return classesValues;
    }

    /**
     * Returns the number of user's tweets for each class
     */
    public int[] getClassesCounter() {
        return classesCounter;
    }

    /**
     * Returns the total number of user's tweets
     */
    public int getTotalTweetsNumber() {
        int totalTweets = 0;

        for (int tweetsNumber : classesCounter) {
            totalTweets += tweetsNumber;
        }

        return totalTweets;
    }

    /**
     * If return true the user account is verified
     */
    public boolean isVerified() {
        return verified;
    }

    /**
     * Override method of comparable interface to set the way to order the users
     * object by total tweets number in decreasing order
     */
    public int compareTo(User user) {
        return (int) (user.getTotalTweetsNumber() - this.getTotalTweetsNumber());
    }
}