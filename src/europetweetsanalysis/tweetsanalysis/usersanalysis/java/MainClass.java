package europetweetsanalysis.tweetsanalysis.usersanalysis.java;

import europetweetsanalysis.utility.java.*;

import java.util.ArrayList;
import weka.core.Instances;

public class MainClass {

	/**
	 * The main store the users class counter in a file and 
	 * print the users stance analysis and the tweets stance analysis 
	 */
	public static void main(String[] args) {
		String inputFilePath = Parameters.CLASSIFIED_TWEETS_FOLDER + Parameters.ARFF_CLASSIFIED_TWEETS_FILE;
		String outputFilePath = Parameters.OUT_PATH_USER_CLASS_COUNTER + Parameters.USERS_CLASS_COUNTER_FILE;
		Instances trainingSet = FileManager.readInstancesFromArff(inputFilePath);

		UsersInformationsManager usersInformationsManager = new UsersInformationsManager(trainingSet, Parameters.CLASSES_VALUES);

		ArrayList<User> usersInfo = usersInformationsManager.getSortedUsersInfo();

		String operationToExecute = args[0];

        switch (operationToExecute) {
            case "storeUsersInformation":
				storeUsersInformations(usersInfo, outputFilePath);
                break;
            case "printStanceAnalysis":
				printUsersStance(usersInformationsManager, Parameters.GET_VERIFIED_USERS);
				printTweetsStance(usersInformationsManager, Parameters.GET_VERIFIED_USERS);
                break;
            default:
                break;
            }
		

		
	}

	private static void printUsersStance(UsersInformationsManager usersInformationsManager, Boolean verified){
		int[] usersStance = usersInformationsManager.getUsersStance(Parameters.USER_THRESHOLD, verified);
		System.out.println("\n======Users stance=======\n");
		for (int i = 0; i < usersStance.length; i++) {
			System.out.println(Parameters.CLASSES_VALUES[i] + ": " + usersStance[i]);
		}
	}

	private static void printTweetsStance(UsersInformationsManager usersInformationsManager, Boolean verified){
		int[] tweetsStance = usersInformationsManager.getTweetsStance(verified);
		System.out.println("\n======Tweets stance=======\n");
		for (int i = 0; i < tweetsStance.length; i++) {
			System.out.println(Parameters.CLASSES_VALUES[i] + ": " + tweetsStance[i]);
		}
	}

	private static void storeUsersInformations(ArrayList<User> usersInfo, String outputFilePath) {
		UsersInformationsStore userInformationsStore = new UsersInformationsStore(outputFilePath);
		userInformationsStore.storeUsersInformation(usersInfo);
	}
}