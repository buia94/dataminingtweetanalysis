package europetweetsanalysis.classificationoftweets.java;

import java.util.ArrayList;

import weka.classifiers.Evaluation;

/**
 * Model class used to store the evaluation result of a classifier
*/
public class EvaluationResults {
    public final String classifierName;
    public final ArrayList<Evaluation> classifierEvaluations;

    public EvaluationResults(String classifierName, ArrayList<Evaluation> classifierEvaluations) {
        this.classifierName = classifierName;
        this.classifierEvaluations = classifierEvaluations;
    }
}