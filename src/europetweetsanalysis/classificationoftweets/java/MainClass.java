package europetweetsanalysis.classificationoftweets.java;

import europetweetsanalysis.utility.java.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.filters.MultiFilter;

public class MainClass {

	/** 
	 * The main performs the operation passed as a parameter (eg evaluate classifier)
	 * The main can be executed for a certain number of repetitions, 
	 * to measure the execution time of each repetition for statistical purposes
	*/
	public static void main(String[] args) {
		String operationToExecute = args[0];
		double[] executionTimesList = new double[Parameters.MAIN_REPETITIONS_NUMBER];
		Instances trainingSet = null;
		Instances notLabeledSet = null; 
		MultiFilter multiFilter = null;

		if(operationToExecute.compareTo("compareClassifiers")!=0){
			trainingSet = FileManager.readInstancesFromArff(Parameters.MANUALLY_LABELED_TWEETS_FOLDER + Parameters.ARFF_LABELED_DATA_FILE);
			notLabeledSet = FileManager.readInstancesFromArff(Parameters.NOT_LABELED_TWEET_FOLDER + Parameters.ARFF_NOT_LABELED_DATA_FILE);
			multiFilter = getMultifilter(trainingSet);
		}

		for (int i = 0; i < Parameters.MAIN_REPETITIONS_NUMBER; i++) {
			long startTime = System.nanoTime();
			System.out.println("\nMain repetition: " + (i+1) + " of " + Parameters.MAIN_REPETITIONS_NUMBER + "\n");

			switch (operationToExecute) {
				case "evaluateClassifiers":
					ArrayList<EvaluationResults> allClassifiersEvaluations = evaluateClassifiers(trainingSet, multiFilter, Parameters.CLASSIFIERS_LIST);
					storeAllClassifiersEvaluation(allClassifiersEvaluations);
					break;
				case "compareClassifiers":
					ClassifiersComparator.storeClassifiersComparison(Parameters.GLOBAL_EVALUATION_METRICS_TO_COMPARE_FOLDER, Parameters.GLOBAL_EVALUATION_METRICS_COMPARISON_FOLDER, Parameters.TEST_NAME);
					ClassifiersComparator.storeClassifiersComparison(Parameters.CLASSES_EVALUATION_METRICS_TO_COMPARE_FOLDER, Parameters.CLASSES_EVALUATION_METRICS_COMPARISON_FOLDER, Parameters.TEST_NAME);
					ClassifiersComparator.storeClassifiersComparison(Parameters.EXECUTION_TIMES_TO_COMPARE_FOLDER, Parameters.EXECUTION_TIMES_COMPARISON_FOLDER, Parameters.TEST_NAME);
					break;
				case "classifyNotLabeledData":
					Instances classifiedInstances = classifyNotLabeledData(trainingSet, multiFilter, notLabeledSet);
					FileManager.printInstancesToArff(classifiedInstances, Parameters.CLASSIFIED_TWEET_FOLDER + Parameters.ARFF_CLASSIFIED_DATA_FILE);
					break;
				default:
					break;
				}
				
				long endTime = System.nanoTime();
				long duration = (endTime - startTime); 
				double durationInSeconds = (double) duration / 1_000_000_000;
				executionTimesList[i] = durationInSeconds;
		}

		System.out.print("Execution times list: ");		//print the execution time for each Main repetition (used for comparison purposes)

		for (double executionTime : executionTimesList) {
			System.out.print(executionTime + " ");
		}
		
		System.out.print("\n");
	}

	/*
	 * Evaluate all classifiers parallelizing the evaluation. The task is
	 * implemented in the Classifiers evaluation class. Set the thread number in the
	 * parameters class
	 */
	private static ArrayList<EvaluationResults> evaluateClassifiers(Instances trainingSet, MultiFilter multiFilter, String[] classifiersList) {
		ArrayList<EvaluationResults> allClassifiersEvaluations = new ArrayList<EvaluationResults>();
		ExecutorService threadPool = Executors.newFixedThreadPool(Parameters.THREADS_NUMBER);
		List<Future<EvaluationResults>> listFuture = new ArrayList<>();
		ArrayList<FilteredClassifier> filteredClassifiersList = getFilteredClassifiersList(multiFilter, classifiersList);

		for (int i = 0; i < filteredClassifiersList.size(); i++) {
			FilteredClassifier filteredClassifier = filteredClassifiersList.get(i);
			ClassifierEvaluator classifierEvaluator = new ClassifierEvaluator(filteredClassifier, classifiersList[i], trainingSet, Parameters.FOLDS_NUMBER, Parameters.CROSS_VALIDATION_SEED);
			listFuture.add(threadPool.submit(classifierEvaluator));
		}

		for (Future<EvaluationResults> evaluation : listFuture) {
			try {
				EvaluationResults classifierEvaluation = evaluation.get();
				allClassifiersEvaluations.add(classifierEvaluation);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		threadPool.shutdown();
		return allClassifiersEvaluations;
	}

	/**
	 * Store the evaluation detail for each tested classifier
	 */
	private static void storeAllClassifiersEvaluation(ArrayList<EvaluationResults> allClassifiersEvaluations) {
		for (EvaluationResults classifierEvaluation : allClassifiersEvaluations) {
			storeSummaryEvaluationResults(classifierEvaluation);
			storeGlobalEvaluationResults(classifierEvaluation);
			storeClassesEvaluationResults(classifierEvaluation);
		}
	}

	/**
	 * Store the summary evaluation for one classifier
	 */
	private static void storeSummaryEvaluationResults(EvaluationResults classifierEvaluation) {
		String additionalInformationFileName = Integer.toString(Parameters.NUM_RELEVANT_ATTRIBUTE);
		String filePath = Parameters.SUMMARY_EVALUATION_METRICS_FOLDER + additionalInformationFileName + "_" + classifierEvaluation.classifierName;
		ClassifierEvaluationStore storeSummaryEvaluation = new ClassifierEvaluationStore(filePath);
		storeSummaryEvaluation.storeSummaryEvaluationMetrics(classifierEvaluation);
	}

	/**
	 * Store the global evaluation (eg accuracy) for one classifier
	 */
	private static void storeGlobalEvaluationResults(EvaluationResults classifierEvaluation) {
		String additionalInformationFileName = Integer.toString(Parameters.NUM_RELEVANT_ATTRIBUTE);
		for (String globalEvaluationMetric : Parameters.GLOBAL_EVALUATION_METRICS_TO_TEST) {
			String filePath = Parameters.GLOBAL_EVALUATION_METRICS_FOLDER + additionalInformationFileName + "_All_" + globalEvaluationMetric;
			ClassifierEvaluationStore storeGlobalEvaluation = new ClassifierEvaluationStore(filePath);
			storeGlobalEvaluation.storeSingleEvaluationMetric(classifierEvaluation, globalEvaluationMetric);
		}
	}

	/**
	 * Store the classes evaluation (eg precision, recall, fscore, auc) for one classifier
	 */
	private static void storeClassesEvaluationResults(EvaluationResults classifierEvaluation) {
		String additionalInformationFileName = Integer.toString(Parameters.NUM_RELEVANT_ATTRIBUTE);
		for (String classEvaluationMetric : Parameters.CLASSES_EVALUATION_METRICS_TO_TEST) {
			for (int classIndex : Parameters.CLASS_INDEXES) {
				String filePath = Parameters.CLASSES_EVALUATION_METRICS_FOLDER + additionalInformationFileName + "_All_" + classEvaluationMetric + "_" + Parameters.CLASS_VALUES[classIndex];
				ClassifierEvaluationStore storeClassesEvaluation = new ClassifierEvaluationStore(filePath);
				storeClassesEvaluation.storeSingleEvaluationMetric(classifierEvaluation, classEvaluationMetric, classIndex);
			}
		}
	}

	/**
	 * Create the filtered classifier using the input multifilter and the classifier written in the Parameters class.
	 * Then build the filtered classifier and call the function to classify instances
	 */
	private static Instances classifyNotLabeledData(Instances trainingSet, MultiFilter multiFilter, Instances notLabeledSet) {
		FilteredClassifier filteredClassifier = getFilteredClassifier(multiFilter, Parameters.CHOSEN_CLASSIFIER);
		buildClassifier(filteredClassifier, trainingSet);
		Instances classifiedInstances = classifyIstances(filteredClassifier, notLabeledSet);
		return classifiedInstances;
	}

	/**
	 * Return the multifilter built using the MultiFilterBuilder class. 
	 * The filters to set are written in the parameters class.
	 */
	private static MultiFilter getMultifilter(Instances trainingSet) {
		MultiFilterBuilder multiFilterBuilder = new MultiFilterBuilder();

		for (String filterName : Parameters.FILTER_LIST) {
            
            switch (filterName) {
                case "attributesToKeep":
					multiFilterBuilder.setAttributesToKeepFilter(Parameters.ATTRIBUTE_RANGE_TO_KEEP);
					break;

				case "stringToWord":
					multiFilterBuilder.setStringToWordFilter(Parameters.MIN_GRAMS, Parameters.MAX_GRAMS, Parameters.SEPARATOR, Parameters.STOPWORDS_FILE, Parameters.NUM_WORDS_TO_KEEP, Parameters.STEM_LANGUAGE);                    
					break;
					
				case "attributeSelection":
					multiFilterBuilder.setAttributeSelectionFilter(Parameters.NUM_RELEVANT_ATTRIBUTE);
                    break;

				case "spreadSubSample":
					multiFilterBuilder.setSpreadSubSampleFilter(Parameters.SUBSAMPLE_FILTER_SEED, Parameters.SUBSAMPLE_FILTER_DISTRIBUTION_SPREAD);
					break;
					
                case "smote":
					for (int i = 0; i < Parameters.SMOTE_CLASS_INDEXES.length; i++) {
						multiFilterBuilder.setSmoteFilter(Parameters.SMOTE_BALANCING_PERCENTAGE[i], Parameters.SMOTE_CLASS_INDEXES[i]); }
					break;
					             
                default:
                    break;
            }
        }

		MultiFilter multiFilter = multiFilterBuilder.getMultiFilter();
		try {
			multiFilter.setInputFormat(trainingSet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return multiFilter;
	}

	/**
	 * Return a list of filtered classifiers object, built using the input multifilter and the 
	 * input classifiers name list
	 */
	private static ArrayList<FilteredClassifier> getFilteredClassifiersList(MultiFilter multiFilter, String[] classifiersList) {
		ArrayList<FilteredClassifier> filteredClassifiersList = new ArrayList<FilteredClassifier>();
		for (String classifierName : classifiersList) {
			FilteredClassifier filteredClassifier = getFilteredClassifier(multiFilter, classifierName);
			filteredClassifiersList.add(filteredClassifier);
		}
		return filteredClassifiersList;
	}

	/**
	 * Return a filtered classifier object, built using the input multifilter and the 
	 * input classifier name 
	 */
	private static FilteredClassifier getFilteredClassifier(MultiFilter multiFilter, String classifierName) {
		ClassifierBuilder classifierBuilder = new ClassifierBuilder();
		AbstractClassifier classifier = classifierBuilder.getClassifier(classifierName);
		FilteredClassifier filteredClassifier = new FilteredClassifier();
		filteredClassifier.setFilter(multiFilter);
		filteredClassifier.setClassifier(classifier);
		return filteredClassifier;
	}

	/**
	 * Build the input filtered classifier, using the input training set
	 */
	private static void buildClassifier(FilteredClassifier filteredClassifier, Instances trainingSet) {
		try {
			System.out.println("\n----------Start building of " + Parameters.CHOSEN_CLASSIFIER + " classifier----------\n");
			filteredClassifier.buildClassifier(trainingSet);
			System.out.println("\nEnd building\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Classify the input not labeled instances using the input filtered classifier (already built).
	 * And than return the classified instances
	 */
	private static Instances classifyIstances(FilteredClassifier filteredClassifier, Instances notLabeledDataset) {
		System.out.println("\n----------Start classification of not labeled dataset with " + Parameters.CHOSEN_CLASSIFIER + " classifier----------\n");
		for (int i = 0; i < notLabeledDataset.numInstances(); i++) {
			double predictedClass;
			try {
				predictedClass = filteredClassifier.classifyInstance(notLabeledDataset.instance(i));
				notLabeledDataset.instance(i).setClassValue(predictedClass);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("\nEnd classification\n");
		return notLabeledDataset;
	}
}
