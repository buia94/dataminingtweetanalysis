package europetweetsanalysis.classificationoftweets.java;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;

public class ClassifierEvaluator implements Callable<EvaluationResults> {

    private FilteredClassifier filteredClassifier;
    private String classifierName; // it is used only for printing purpose
    private Instances trainingSet;
    private int foldsNumber;
    private int[] seedsList;
    private ArrayList<Evaluation> classifierEvaluations;

    public ClassifierEvaluator(FilteredClassifier filteredClassifier, String classifierName, Instances trainingSet,
            int foldsNumber, int[] seedsList) {
        this.filteredClassifier = filteredClassifier;
        this.trainingSet = trainingSet;
        this.foldsNumber = foldsNumber;
        this.seedsList = seedsList;
        this.classifierName = classifierName;
        classifierEvaluations = new ArrayList<Evaluation>();
    }

    public EvaluationResults call() {
        EvaluationResults evaluationResults = executeAllCrossValidationRepetitions();
        return evaluationResults;
    }

    /**
     * Execute all the cross validation repetitions for all the seeds and return the
     * evaluation results for a single classifier
     */
    private EvaluationResults executeAllCrossValidationRepetitions() {
        System.out.println("\n======== Cross validation with " + foldsNumber + " folds. Classifier name: " + classifierName + " =========");
        for (int j = 0; j < seedsList.length; j++) {
            System.out.println("\n ------ START. Classifier name: " + classifierName + ". Repetition number: " + (j+1) + " --------");
            
            ArrayList<Evaluation> foldsEvaluationList = getFoldsEvaluationList(seedsList[j]);
            classifierEvaluations.addAll(foldsEvaluationList);

            System.out.println("\n ------ END. Classifier name: " + classifierName + ". Repetition number: " + (j+1) + " --------");
        }
        EvaluationResults evaluationResults = new EvaluationResults(classifierName, classifierEvaluations);
        return evaluationResults;
    }

    /**
     * Execute a single cross validation repetition with the seed given in input.
     * Return an evaluation for each fold
     */
    private ArrayList<Evaluation> getFoldsEvaluationList(int seed) {
        Instances data = new Instances(trainingSet);

        ArrayList<Evaluation> evaluationList = new ArrayList<Evaluation>();

        try {
            Random rand = new Random(seed);
            data.randomize(rand); // randomize the data
            data.stratify(foldsNumber); // stratify the randomized data for 10-fold CV

            for (int foldIndex = 0; foldIndex < foldsNumber; foldIndex++) {
                Evaluation eval = new Evaluation(data);
                Instances trainData = data.trainCV(foldsNumber, foldIndex, rand);
                Instances testData = data.testCV(foldsNumber, foldIndex);
                Classifier copiedClassifier = AbstractClassifier.makeCopy(filteredClassifier);
                copiedClassifier.buildClassifier(trainData);
                eval.evaluateModel(copiedClassifier, testData);
                evaluationList.add(eval);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return evaluationList;
    }

    /**
     * Execute a single cross validation repetition with the seed given in input.
     * All The folds evaluation are aggregated
    */
    private Evaluation getAggregatedFoldsEvaluation(int seed) {
        Evaluation evaluation = null;
        try {
            evaluation = new Evaluation(trainingSet);
            evaluation.crossValidateModel(filteredClassifier, this.trainingSet, foldsNumber, new Random(seed));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return evaluation;
    }

}