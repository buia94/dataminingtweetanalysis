package europetweetsanalysis.classificationoftweets.java;

public class Parameters {

    //================================= PATH PARAMETERS ============================================
	final static String RESOURCES_PATH = "./resources/Europe_tweets_analysis/";
	final static String RESOURCES_PATH_CLASSIFICATION_OF_TWEET = RESOURCES_PATH + "Classification_of_tweets/";
	
	final static String STOPWORDS_FILE = RESOURCES_PATH_CLASSIFICATION_OF_TWEET + "stopwords.txt";
	//-----------------------------//
	final static String OUT_PATH = "./out/Europe_tweets_analysis/";
	final static String OUT_PATH_CLASSIFICATION_OF_TWEET = OUT_PATH + "Classification_of_tweets/";
	final static String OUT_PATH_TWEETS_PREPROCESSING = OUT_PATH + "Tweets_preprocessing/";

	final static String MANUALLY_LABELED_TWEETS_FOLDER = OUT_PATH_TWEETS_PREPROCESSING + "cleaned_tweets/manually_labeled_tweets/";
	final static String NOT_LABELED_TWEET_FOLDER = OUT_PATH_TWEETS_PREPROCESSING + "cleaned_tweets/not_labeled_tweets/";
	final static String CLASSIFIED_TWEET_FOLDER = OUT_PATH_CLASSIFICATION_OF_TWEET + "classified_tweets/";
	
	final static String ARFF_LABELED_DATA_FILE = "final_manually_labeled_tweet.arff";
	final static String ARFF_NOT_LABELED_DATA_FILE = "not_labeled_tweet.arff";

	final static String ARFF_CLASSIFIED_DATA_FILE = "classified_tweet.arff";	
	final static String EVALUATION_METRICS_FOLDER = OUT_PATH_CLASSIFICATION_OF_TWEET + "classifiers_evaluation_metrics/";
	final static String SUMMARY_EVALUATION_METRICS_FOLDER = EVALUATION_METRICS_FOLDER + "summary/";
	final static String GLOBAL_EVALUATION_METRICS_FOLDER = EVALUATION_METRICS_FOLDER + "global_evaluation_metrics/";
	final static String CLASSES_EVALUATION_METRICS_FOLDER = EVALUATION_METRICS_FOLDER + "classes_evaluation_metrics/";
	final static String EXECUTION_TIMES_FOLDER = EVALUATION_METRICS_FOLDER + "execution_times/";

	final static String GLOBAL_EVALUATION_METRICS_TO_COMPARE_FOLDER = GLOBAL_EVALUATION_METRICS_FOLDER + "Classifiers_to_compare/";
	final static String CLASSES_EVALUATION_METRICS_TO_COMPARE_FOLDER = CLASSES_EVALUATION_METRICS_FOLDER + "Classifiers_to_compare/";
	final static String EXECUTION_TIMES_TO_COMPARE_FOLDER = EXECUTION_TIMES_FOLDER + "Classifiers_to_compare/";


	final static String CLASSIFIER_COMPARISON_FOLDER = OUT_PATH_CLASSIFICATION_OF_TWEET + "classifiers_comparison/";
	final static String GLOBAL_EVALUATION_METRICS_COMPARISON_FOLDER = CLASSIFIER_COMPARISON_FOLDER + "global_evaluation_metrics/";
	final static String CLASSES_EVALUATION_METRICS_COMPARISON_FOLDER = CLASSIFIER_COMPARISON_FOLDER + "classes_evaluation_metrics/";
	final static String EXECUTION_TIMES_COMPARISON_FOLDER = CLASSIFIER_COMPARISON_FOLDER + "execution_times/";

	//---------------SCRIPT R--------------//
	static final String R_FOLDER_PATH = "./src/europetweetsanalysis/classificationoftweets/R/";
	static final String R_SCRIPT_WILCOXON_TEST = R_FOLDER_PATH + "wilcoxonScript.R";
	//===============================================================================================

    //====================================== MULTI FILTER PARAMETERS =================================
	final static String STEM_LANGUAGE = null;	//in order to don't do the stemming set to null. For italian stem set "italian"
	final static int MAX_GRAMS = 2;
	final static int MIN_GRAMS = 1;
	final static String SEPARATOR = " \r\n" + " 	.,;:'\"()?!";
	final static double[] SMOTE_BALANCING_PERCENTAGE = {35,35};
	final static int[] SMOTE_CLASS_INDEXES = {1,2};
	final static String ATTRIBUTE_RANGE_TO_KEEP = "first,last";
	final static int NUM_WORDS_TO_KEEP = 3000;		//number of unique sword to keep per class in weka. Obbligatory to set (default: 1000)
	final static int NUM_RELEVANT_ATTRIBUTE = -1;	//to mantain all attributes set to -1
	final static int SUBSAMPLE_FILTER_SEED = 10;
	final static double SUBSAMPLE_FILTER_DISTRIBUTION_SPREAD = 1.0;		//to balance perfectly set to 1.0

	final static String[] FILTER_LIST = {"attributesToKeep", "stringToWord", "attributeSelection"};
	//===============================================================================================
	
	//===============================CLASSIFICATOR PARAMETERS=======================================
	final static String[] CLASSIFIERS_LIST = {"SimpleLogistic","SMO", "NaiveBayes", "J48", "NaiveBayesMultinomial"};
	final static String CHOSEN_CLASSIFIER = "SMO";
	final static String SMO_KERNEL = "PolyKernel";
	//===============================================================================================

	//===============================EVALUATION PARAMETERS========================================
	final static int FOLDS_NUMBER = 10;
	final static int[] CROSS_VALIDATION_SEED = {1, 2, 3};
	final static String[] GLOBAL_EVALUATION_METRICS_TO_TEST = {"accuracy"};
	final static String[] CLASSES_EVALUATION_METRICS_TO_TEST = {"ROC", "fscore"};
	final static int[] CLASS_INDEXES = {0,1,2};
	final static String[] CLASS_VALUES = {"pro", "contro", "neutro"};
	//===============================================================================================

	//===============================COMPARISON PARAMETERS==========================================
	final static String TEST_NAME = "Wilcoxon";
	//===============================================================================================

	//=============================== MAIN PARAMETERS ====================================
	final static int THREADS_NUMBER = 5;
	final static int MAIN_REPETITIONS_NUMBER = 1; //repetiotions of main executions. Used for statistics purposes on execution time
	//==============================================================================================

}