package europetweetsanalysis.classificationoftweets.java;
import europetweetsanalysis.utility.java.*;

public class ClassifiersComparator{

	/**
	 * Call the executeTest method and store in a file the output of test 
	*/
    public static void storeClassifiersComparison(String inputEvaluationMetricsFolder, String outputComparisonFolder, String testName){
		String[] filesNames = FileManager.readAllFileName(inputEvaluationMetricsFolder);
		for (String fileName : filesNames) {
			String evaluationMetricsFile = inputEvaluationMetricsFolder + fileName;
			String commandOutput = executeTest(evaluationMetricsFile, testName);
			String outpuFilePath = outputComparisonFolder + fileName;
			boolean append = false;
			FileManager.printStringToFile(commandOutput, outpuFilePath, append);
		}
	}

	/**
	 * Execute the test given in input, to compare the evaluation metrics of different classifiers
	*/
	private static String executeTest(String evaluationMetricsFile, String testName){
		String command = getBashCommand(testName);
		boolean printErrorStream = false;
		String commandOutput = BashExecutor.executeScript(command + " " + evaluationMetricsFile, printErrorStream);
		return commandOutput;
	}

	/**
	 * Return the bash command to execute the comparison test given in input
	*/
	private static String getBashCommand(String testName){
		String command = "";
		switch (testName) {
            case "Wilcoxon":
                command = "Rscript " + Parameters.R_SCRIPT_WILCOXON_TEST; 
                break;
            default:
                break;
		}
		return command;
    }
}