package europetweetsanalysis.classificationoftweets.java;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.supportVector.Kernel;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.meta.AdaBoostM1;

/** 
 * The classifiers configuration will be put in the file Parameters.java
*/
public class ClassifierBuilder{

    public AbstractClassifier getClassifier(String classifierName) {
        AbstractClassifier classifier = null;
        switch (classifierName) {
        case "J48":
            classifier = getDecisionTree();
            break;
        case "NaiveBayes":
            classifier = getNaiveBayes();
            break;
        case "NaiveBayesMultinomial":
            classifier = getNaiveBayesMultinomial();
            break;
        case "RandomForest":
            classifier = getRandomForest();
            break;
        case "SimpleLogistic":
            classifier = getSimpleLogistic();
            break;
        case "SMO":
            classifier = getSupportVectorMachines();
            break;
        case "AdaBoost":
            classifier = getAdaBoost();
            break;
        default:
            break;
        }
        return classifier;
    }

    public Kernel getKernel(String kernelName){
        Kernel ker = null;
        switch (kernelName) {
            case "PolyKernel":
                ker = new PolyKernel();
                break;
            case "RBFKernel":
                ker = new RBFKernel();
                break;
            default:
                ker = new PolyKernel();
            break;
        }
        return ker;

    }

    public J48 getDecisionTree(){
        J48 decisionTreeClassifier = new J48();
        return decisionTreeClassifier;
    }

    public NaiveBayes getNaiveBayes(){
        NaiveBayes naiveBayesianClassifier = new NaiveBayes();
        return naiveBayesianClassifier;
    }

    public NaiveBayesMultinomial getNaiveBayesMultinomial(){
        NaiveBayesMultinomial naiveBayesMultinomialClassifier = new NaiveBayesMultinomial();
        return naiveBayesMultinomialClassifier;
    }

    public RandomForest getRandomForest(){
        RandomForest randomForestClassifier = new RandomForest();
        return randomForestClassifier;
    }

    public SimpleLogistic getSimpleLogistic(){
        SimpleLogistic simpleLogisticClassifier = new SimpleLogistic();
        return simpleLogisticClassifier;
    }

    public SMO getSupportVectorMachines(){
        SMO supportVectorMachinesClassifier = new SMO();
        Kernel kernel = getKernel(Parameters.SMO_KERNEL);
        supportVectorMachinesClassifier.setKernel(kernel);
        return supportVectorMachinesClassifier;
    } 

    public AdaBoostM1 getAdaBoost(){
        AdaBoostM1 adaBoostClassifier = new AdaBoostM1();
        return adaBoostClassifier;
    }

}