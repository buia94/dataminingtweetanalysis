package europetweetsanalysis.classificationoftweets.java;

import europetweetsanalysis.utility.java.*;

import java.util.ArrayList;

import weka.classifiers.AggregateableEvaluation;
import weka.classifiers.Evaluation;

public class ClassifierEvaluationStore {
    private String outputFilePath;

    public ClassifierEvaluationStore(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    /**
     * Store a single evaluation metric (eg accuracy, ROC curve, f_score) for every
     * repetitions. It is used a single file containing the metrics for all
     * classifier
     */
    public void storeSingleEvaluationMetric(EvaluationResults classifierResult, String evaluationMetricType, int classIndex) {
        StringBuilder stringToStore = new StringBuilder("");
        stringToStore.append(classifierResult.classifierName + " ");
        for (int i = 0; i < classifierResult.classifierEvaluations.size(); i++) {
            Evaluation eval = classifierResult.classifierEvaluations.get(i);
            String evaluationMetricValue = getEvaluationMetricValue(eval, evaluationMetricType, classIndex);
            stringToStore.append(evaluationMetricValue + " ");
        }
        stringToStore.append("\n");
        boolean append = true;
        FileManager.printStringToFile(stringToStore.toString(), outputFilePath, append);
    }

    public void storeSingleEvaluationMetric(EvaluationResults classifierResult, String evaluationMetricType) {
        storeSingleEvaluationMetric(classifierResult, evaluationMetricType, -1);
    }

    /**
     * Store the summary evaluation metrics of each repetion. It is used a different
     * file for each classifier
     */
    public void storeSummaryEvaluationMetrics(EvaluationResults classifierResult) {
        String summaryEvaluationMetrics = getSummaryEvaluationMetrics(classifierResult.classifierEvaluations);
        boolean append = false;
        FileManager.printStringToFile(summaryEvaluationMetrics, outputFilePath, append);
    }

    /**
     * Aggregate all Evaluation and return the summary of the evaluation metrics 
    */
    private String getSummaryEvaluationMetrics(ArrayList<Evaluation> classifierEvaluations) {        
        StringBuilder evaluationMetrics = new StringBuilder("");

        if (classifierEvaluations.size() == 0) {
            throw new RuntimeException("Nothing to aggregate: classifier evaluations array is empty");
          }

        try {
            Evaluation firstEvaluation = classifierEvaluations.get(0);
            AggregateableEvaluation aggregateEvaluation = new AggregateableEvaluation(firstEvaluation);
            for (int i = 0; i < classifierEvaluations.size(); i++) {
                aggregateEvaluation.aggregate(classifierEvaluations.get(i));
            }

            evaluationMetrics.append(aggregateEvaluation.toSummaryString() + "\n\n");
            evaluationMetrics.append(aggregateEvaluation.toMatrixString() + "\n\n");
            evaluationMetrics.append(aggregateEvaluation.toClassDetailsString() + "\n\n");
        } 
        catch (Exception e1) {
            e1.printStackTrace();
        }
        return evaluationMetrics.toString();
    }

    /**
     * Returns the value for an evaluation metric type given in input. If the
     * evaluation metric type is the accuracy, there is no need to use the
     * classIndex
     */
    private String getEvaluationMetricValue(Evaluation eval, String evaluationMetricType, int classIndex) {
        String evaluationMetricValue = "";
        switch (evaluationMetricType) {
        case "accuracy":
            evaluationMetricValue = String.valueOf(eval.pctCorrect());
            break;
        case "ROC":
            evaluationMetricValue = String.valueOf(eval.areaUnderROC(classIndex));
            break;
        case "fscore":
            evaluationMetricValue = String.valueOf(eval.fMeasure(classIndex));
            break;
        default:
            break;
        }
        return evaluationMetricValue;
    }
}