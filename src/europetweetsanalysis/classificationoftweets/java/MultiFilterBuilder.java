package europetweetsanalysis.classificationoftweets.java;

import java.io.File;
import java.util.ArrayList;

import weka.core.stemmers.SnowballStemmer;
import weka.core.stopwords.WordsFromFile;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.Filter;
import weka.filters.MultiFilter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.filters.supervised.instance.SMOTE;
import weka.filters.supervised.instance.SpreadSubsample;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;

public class MultiFilterBuilder {
    private ArrayList<Filter> filtersList;

    /*
     * This class is used to create a MultiFilter composed by several filter setted
     * using the different class methods
     */
    public MultiFilterBuilder() {
        filtersList = new ArrayList<Filter>();
    }

    /**
     * Return the multifilter built using the filter list.
     * The filter list is setted using all functions below
     */
    public MultiFilter getMultiFilter() {
        MultiFilter multiFilter = new MultiFilter();
        multiFilter.setFilters(filtersList.toArray(new Filter[filtersList.size()]));
        return multiFilter;
    }

    /**
     * Set the string to word filter and put in the filter list
     */
    public void setStringToWordFilter(int minSizeNGram, int maxSizeNGram, String separator, String stopWordFile, int numWordToKeep, String stemLanguage) {
        StringToWordVector stringToWordFilter = new StringToWordVector();
        NGramTokenizer tokenizer = getTokenizer(minSizeNGram, maxSizeNGram, separator);
        WordsFromFile stopWordsFilter = getStopWordFilter(stopWordFile);
        if (stemLanguage != null) {
            SnowballStemmer stemmer = getStemmer(stemLanguage);
            stringToWordFilter.setStemmer(stemmer);
            System.out.println("\n===Setted stemming===");
        }

        stringToWordFilter.setIDFTransform(true);
        stringToWordFilter.setTFTransform(true);
        stringToWordFilter.setTokenizer(tokenizer);
        stringToWordFilter.setStopwordsHandler(stopWordsFilter);
        stringToWordFilter.setLowerCaseTokens(true);
        stringToWordFilter.setWordsToKeep(numWordToKeep);

        filtersList.add(stringToWordFilter);
        System.out.println("\n===Setted string to word filter===\n");
    }

    /**
     * Set the string to word filter without stemming and put in the filter list
     */
    public void setStringToWordFilter(int minSizeNGram, int maxSizeNGram, String separator, String stopWordFile,
            int numWordToKeep) {
        setStringToWordFilter(minSizeNGram, maxSizeNGram, separator, stopWordFile, numWordToKeep, null);
    }

    /**
     * Set the attributes to keep filter and put in the filter list
     */
    public void setAttributesToKeepFilter(String range) {
        Remove removeFilter = new Remove();
        removeFilter.setAttributeIndices(range);
        removeFilter.setInvertSelection(true);
        filtersList.add(removeFilter);
        System.out.println("\n===Setted attributes to keep filter===\n");

    }

    /**
     * Set the spread subsample filter (it does undersampling) and put in the filter list
     * Note: If the distribution spread is setted to 1.0, the dataset become completely balanced
     */
    public void setSpreadSubSampleFilter(int seed, double distributionSpread) {
        SpreadSubsample underSampleFilter = new SpreadSubsample();
        underSampleFilter.setSeed(seed);
        underSampleFilter.setDistributionSpread(distributionSpread);
        filtersList.add(underSampleFilter);
        System.out.println("\n===Setted spread subsample filter===\n");

    }

    /**
     * Set the attribute selection filter (it does stem filtering) and put in the filter list
     */
    public void setAttributeSelectionFilter(int numRelevantAttributes) {
        AttributeSelection attributeSelector = new AttributeSelection();
        try {
            Ranker searchMethod = new Ranker();
            searchMethod.setNumToSelect(numRelevantAttributes);
            InfoGainAttributeEval attributeEvaluator = new InfoGainAttributeEval();
            attributeSelector.setEvaluator(attributeEvaluator);
            attributeSelector.setSearch(searchMethod);

        } catch (Exception e) {
            e.printStackTrace();
        }
        filtersList.add(attributeSelector);
        System.out.println("\n===Setted attribute selection filter===\n");

    }

    /**
     * Set the smote filter (it does oversampling) and put in the filter list
     */
    public void setSmoteFilter(double balancingPercentage, int classIndex) {
        SMOTE smoteFilter = new SMOTE();
        smoteFilter.setClassValue(String.valueOf(classIndex));
        smoteFilter.setPercentage(balancingPercentage);
        filtersList.add(smoteFilter);
        System.out.println("\n===Setted SMOTE filter===\n");
    }

    private NGramTokenizer getTokenizer(int min, int max, String separator) {
        NGramTokenizer tokenizer;
        tokenizer = new NGramTokenizer();
        tokenizer.setNGramMaxSize(max);
        tokenizer.setNGramMinSize(min);
        tokenizer.setDelimiters(separator);
        return tokenizer;
    }

    private WordsFromFile getStopWordFilter(String stopWordFile) {
        WordsFromFile stopWordsFilter;
        stopWordsFilter = new WordsFromFile();
        stopWordsFilter.setStopwords(new File(stopWordFile));
        return stopWordsFilter;
    }

    private SnowballStemmer getStemmer(String stemLanguage) {
        SnowballStemmer stemmer;
        stemmer = new SnowballStemmer(stemLanguage);
        return stemmer;
    }

}