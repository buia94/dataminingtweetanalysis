# Put the statistics file inside this folder and run the python script to calculate the confidence interval
# The name of the file should be passed as argument to the script
# Run example: python3 confidence_interval_calculator.py All_Accuracy 
# Note: to calculate the confidence interval we need independence.

import sys
from scipy.stats import sem, t
from scipy import mean

def is_float(string):
  try:
    return float(string) and '.' in string  # True if string is a number contains a dot
  except ValueError:  # String is not a number
    return False


if __name__ == "__main__":
    fileName = sys.argv[1]
    confidence = 0.95

    file = open(fileName, "r")

    for line in file:
  
        fields = line.split(" ")

        count = 0
        name = ""
        data = []

        for field in fields:
            if(count==0):
                name = field
            elif(is_float(field)):
                data.append(float(field))
            count = count + 1

        n = len(data)
        m = mean(data)
        std_err = sem(data)
        h = std_err * t.ppf((1 + confidence) / 2, n - 1)
        start = m - h
        end = m + h
    
        print(name + ": [" + str(start) + ", " + str(m) + ", " + str(end) + "] increment=" + str(h))

    file.close()




