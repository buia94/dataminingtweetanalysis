The topic of the project is the analysis of tweets to determine if an italian person is 
favorable, neutral or contrary to European Union. We will use different data mining approach.


USE CASE:
1)  Download the tweets using the MainClass in collectionoftweets package. 
    Set query and various parameters in the Parameter class.

2)  Preprocess the extracted tweets using the MainClass in tweetspreprocessing package. The provided operation (in order
    of execution) are:
    - Parse tweets: select only some tweets fields and prints the file in the same format. (done with a awk script)
    - Sample tweets: split the tweet data set into two samples. One sample to be labeled manually and another 
      to be labeled with the classifier
    - Clean tweets: clean the tweets removing special characters, link, mentions ecc. (done with a awk script)
    - Merge tweets: merge several tweet file removing duplicate tweets

    Pass the parameters at the MainClass with the requested operation. 
    Request only the necessary operations.
    Set file names and various parameters in the Parameter class.

3)  Try, evaluate and compare all the classifier using the MainClass in classificationoftweets package. 
    The training set to train the classifier must be created before. 
    The provided operation are:
    - Evaluate classifiers: evaluate all classifiers using the same multifilter (stringToWordVector, AttributeSelection ecc). 
      We set classifiers and filter in the Parameters class. All evaluation metrics are stored in different files.
    - Compare classifiers: Compare an evaluation metric of different classifiers, using the wilcoxon signed rank test. 
      (done with a awk script). Put the evaluation to compare in the Classifiers_to_compare/ folder.
    - Classify not labeled data: Classify not labeled data, using the best classifier.

    Set file names and various parameters in the Parameter class.

4)  Analyze the tweets previously classified by the best classifier. There is 2 type of analysis:
    - Users analysis: performs the users stance analysis and the tweets stance analysis
    - Peak analysis: performs the peak analysis collecting information on the number of tweets for each single day

    Set file names and various parameters in the Parameter class.
